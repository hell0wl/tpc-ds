#!/bin/bash


echo 1;

DATA_DIR=/opt/tpcdsdata;

echo ${DATA_DIR};

for table_name in `ls ${DATA_DIR} | cut -d . -f 1`; do

    echo "导入Table表名称 ：  " ${table_name};

    file_name=${DATA_DIR}/${table_name}.dat;

    echo "导入文件路径 ：  " ${file_name};

    upload_data_sql="INSERT INTO $table_name FORMAT CSV";

    echo "$upload_data_sql";

    cat $file_name | clickhouse-client --format_csv_delimiter="|" --max_partitions_per_insert_block=100 --database="tpcds" --query="$upload_data_sql"

    #sleep 5
done